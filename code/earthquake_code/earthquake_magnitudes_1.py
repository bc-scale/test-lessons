# -*- coding: utf-8 -*-
"""
Script to write a report on daily earthquake activity based on USGS data.

This version is a single, non-Pythonic script.
"""
from collections import Counter
from datetime import datetime
from pathlib import Path

from eq_report_utils import load_csv_data

# See https://earthquake.usgs.gov/earthquakes/feed/v1.0/csv.php for details
DAILY_EQS_URL = "https://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/all_day.csv"  # noqa
CSV_FILE = Path(__file__).parent / "usgs_eqs_all_day_example.csv"

# The load_csv_data function is defined in eq_report_utils.py
# It downloads and caches a csv file of data, then reads it into a list.
# Read the source code to learn about:
# - using Path to handle files
# - using requests module to transfer data via HTTP
# - next() to advance iterators
# - _ as a placeholder for unwanted variables
raw_data, last_updated = load_csv_data(DAILY_EQS_URL, CSV_FILE)


# Prepare data
data = []
for row in raw_data:
    # Extract useful data
    timestamp = row[0]
    latitude = row[1]
    longitude = row[2]
    depth = row[3]
    mag = row[4]

    # Convert to Python data types
    converted_row = (datetime.strptime(timestamp, '%Y-%m-%dT%H:%M:%S.%fZ'),
                     float(latitude),
                     float(longitude),
                     float(depth),
                     float(mag),
                     )

    # We don't expect anything greater than magnitude 9 so the he following
    # line will crash the program to warn the user.
    assert float(mag) < 10, "Can't handle magnitude greater than 9."

    # Write to new list, discarding magnitude < 0
    if float(mag) > 0:
        data.append(converted_row)


# Find largest earthquake
largest = 0
for row in data:
    mag = row[4]
    if mag > largest:
        largest = mag


# Classify and count magnitudes
def classify_magnitude(mag):
    """
    Assign numerical magnitude to size category.
    """
    if 0 <= mag < 1:
        category = "0-1"
    elif 1 <= mag < 2:
        category = "1-2"
    elif 2 <= mag < 3:
        category = "2-3"
    elif 3 <= mag < 4:
        category = "3-4"
    elif 4 <= mag < 5:
        category = "4-5"
    elif 5 <= mag < 6:
        category = "5-6"
    elif 6 <= mag < 7:
        category = "6-7"
    elif 7 <= mag < 8:
        category = "7-8"
    elif 8 <= mag < 9:
        category = "8-9"
    else:
        category = "> 9"

    return category


magnitudes = []
for row in data:
    magnitudes.append(classify_magnitude(row[4]))

magnitude_counts = Counter(magnitudes)


# Write report
outfile = open('magnitude_report.txt', 'w')

outfile.write(f"Earthquake Magnitude Report for "
              f"{datetime.now().date().isoformat()}\n\n")
outfile.write(f"Data updated: {last_updated}\n")
outfile.write(f"Largest: {largest}\n")

outfile.write(f"\nMagnitude counts:\n")
for mag_class in sorted(magnitude_counts.keys()):
    outfile.write(f"{mag_class}: {magnitude_counts[mag_class]: 3n}\n")

outfile.close()
