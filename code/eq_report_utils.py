# -*- coding: utf-8 -*-
"""
Utility functions for downloading and reading USGS earthquake data.
"""
import csv
from datetime import datetime
from pathlib import Path

import requests


def load_csv_data(url, target_file):
    """
    Load data from csv file, downloading from url if it doesn't exist.

    :return data, last_updated: list of list of CSV data, excluding header row,
                                datetime object with last update time of file
    """
    last_updated = download_text_file_once(url, target_file)
    data = read_csv_data(target_file)

    return data, last_updated


def download_text_file_once(url, target_file):
    """
    Download text file from given URL, only if it doesn't already exist.

    :return timestamp: datetime, the creation time of the text file.
    """
    text_file = Path(target_file)
    if not text_file.exists():
        response = requests.get(url)
        text_file.write_text(response.text)

    timestamp = datetime.fromtimestamp(text_file.stat().st_ctime)
    return timestamp


def read_csv_data(csv_file, header_lines=1):
    """
    Read csv data, discarding header lines.

    :return data: List of data rows (lists)
    """
    with open(csv_file) as infile:
        reader = csv.reader(infile)

        # Discard header rows
        for i in range(header_lines):
            _ = next(reader)

        data = [row for row in reader]

    return data
